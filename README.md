# ETH-Gitlab-opdracht

## Installation

1. Clone Repository
2. `cd ETH-Gitlab-opdracht`
3. Docker build -t [myImageName] .
4. Docker run -p [PORT]:80 -d --name [containerName] [imageName]

curl 127.0.0.1:[PORT] to test if the image is up.
